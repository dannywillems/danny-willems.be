danny-willems.be
================

My multi-language portfolio (https://danny-willems.be).

It's based on the starter bootstrap template **Creative**
(http://startbootstrap.com/template-overviews/creative/). Thanks to his creator.

I changed the theme color to #7070FF and add a dw.less file for other css
classes such as cards.

A gulpfile is provided and you can use **npm install** to install dev
dependancies.

My portfolio is available in French and English. I use php variables, default
browser language and get paramter to let the user choose his prefered language.
A tutorial about the method will be available on my blog:
https://blog.danny-willems.be.

Version 2.2
===========
- Skills and Education section.
- Add a parameter in the card factory to add a class to the main div containing
  the card.

Version 2.1
===========
- Add a dropdown menu to allow user to change the language. Based on a GET
  parameter, save it in a cookie for 30 days.

Version 2.0
===========
- Add multi-langage translation.
- Cards are created with a PHP function
